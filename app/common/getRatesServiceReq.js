var request=
{
          "fromAddress": {
          "company": "Pitney Bowes Inc.",
          "name": "sender_fname",
          "phone": "2032032033",
          "email": "sender@email.com",
          "residential": true,
          "addressLines": [
            "27 Waterview Drive"
          ],
          "cityTown": "Shelton",
          "stateProvince": "CT",
          "postalCode": "06484",
          "countryCode": "US",
          "status": "NOT_CHANGED"
        },
        "toAddress": {
          "company": "Glorias Co.",
          "name": "Peter",
          "phone": "2222222222",
          "email": "receiver@email.com",
          "residential": true,
          "addressLines": [
            "1 Sullivan SQ"
          ],
          "cityTown": "Berwick",
          "postalCode": "03901",
          "countryCode": "US",
          "status": "NOT_CHANGED"
        },
          "parcel": {
              "weight": {
                  "unitOfMeasurement": "OZ",
                  "weight": 1
              },
              "dimension": {
                  "unitOfMeasurement": "IN",
                  "length": 5,
                  "width": 0.25,
                  "height": 4.0,
                  "irregularParcelGirth": 1.0
              }
          },
          "rates": [
              {
                  "carrier": "USPS",
                  "parcelType": "PKG",
                  "inductionPostalCode": "06484"
              }
          ]
      }
  