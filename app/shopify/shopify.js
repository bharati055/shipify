'use strict';

angular.module('myApp.shopify', ['ngRoute','ui.bootstrap','ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/shopify', {
    templateUrl: 'shopify/shopify.html',
    controller: 'ShopifyCtrl'
  });
}])

.factory('shippingRatesSvc', [  
  function(){
    return {
      shippingRatesData : { "rates": []}
    }
  }

])

.factory('getRatesReqSvc', function($resource) {
  return $resource('../common/getRatesServiceReq.json',{row:'@row'});
  
  // $resource('../common/getRatesServiceReq.json')
  // .$promise.then(function(response){  
  //  var data =response.body
  //  return data ;
      
  //   },function(response){
  //     console.log('ERROR !!!');
  //       console.log(response);
  //   });


})

.controller('ShopifyCtrl', ['$scope','shopifySvc','shippingRatesSvc','$http','$location','getRatesReqSvc',
function($scope,shopifySvc,shippingRatesSvc,$http,$location,getRatesReqSvc) {

  $scope.orderData = shopifySvc.shopifyData.orders;

  $scope.tables = [];

  $scope.rate_toSpin=null;
  $scope.ship_toSpin=null;
  $scope.isRateSelected = null;
  $scope.selectedOrderIndex = null;
  $scope.selectedRateIndex = null;
  $scope.shipmentId = "USPS2200080166654758";
  $scope.parcelTrackingNumber = "9405509898642004076859";
  $scope.pdfURL = "https://web-prt3.gcs.pitneybowes.com/usps/325584758/outbound/label/662192e9d98446f986c8e9b18ab87f99.pdf";
  $scope.showShipmentDetails = null;
  $scope.getRateReq_final = null;
  $scope.orderStatus = 'Open';
  
  
  $scope.setSelected = function (isRateSelected) {
     $scope.isRateSelected = isRateSelected;
     console.log('Selected rates row - '+ isRateSelected);
     
  };

  $scope.cancelSelection = function () {
    $scope.isRateSelected = null;
    $scope.showShipmentDetails = null;
 };

 $scope.setCurrentOrderIndex = function(index){
  $scope.selectedOrderIndex = index;
  console.log('Selected Order Index - '+ $scope.selectedOrderIndex);
};

 $scope.setCurrentRateIndex = function(index){
    $scope.selectedRateIndex = index;
    console.log('Selected Rate Index - '+ $scope.selectedRateIndex);
 };

 $scope.toggle = function(account,isSelected,idx){
      $scope.selectedUser = isSelected?account : {};
          if(isSelected){
          $scope.tables.forEach(function(item,index){
              if(index !== idx){
              item.showDetails = false;
              }
        });
    }
}


 
 $scope.generateRequest_GetRates = function(){
      
      $scope.rate_toSpin=true;
      var data = getRatesReqSvc.get();
      
      data.$promise.then(function(response){  

        $scope.gatRates_request = response.body;
        console.log('GetRates request >>> ')
        console.log($scope.gatRates_request);
        console.log($scope.orderData);

        var rate = $scope.gatRates_request;
        var order = $scope.orderData[$scope.selectedOrderIndex];

        console.log(rate);
        console.log(order);
        console.log(rate.fromAddress.company+'---------------' + order.billing_address.company)

        rate.fromAddress.company=order.billing_address.company
        rate.fromAddress.name=order.billing_address.name
        rate.fromAddress.phone=order.billing_address.phone
        rate.fromAddress.email=order.email
        rate.fromAddress.addressLines[0]=order.billing_address.address1
        rate.fromAddress.cityTown=order.billing_address.city
        rate.fromAddress.stateProvince=order.billing_address.province
        rate.fromAddress.postalCode=order.billing_address.zip
        rate.fromAddress.countryCode="US"//order.billing_address.company
        
        rate.toAddress.company=order.shipping_address.company
        rate.toAddress.name=order.shipping_address.name
        rate.toAddress.phone=order.shipping_address.phone
        rate.toAddress.email=order.shipping_address.contact_email
        rate.toAddress.addressLines[0]=order.shipping_address.address1
        rate.toAddress.cityTown=order.shipping_address.city
        rate.toAddress.stateProvince=order.shipping_address.province
        rate.toAddress.postalCode=order.shipping_address.zip
        rate.toAddress.countryCode="US"//order.shipping_address.company
        
        rate.parcel.weight.unitOfMeasurement= "GM"
        rate.parcel.weight.weight=order.line_items[0].grams
        //rate.parcel.dimension.unitOfMeasurement
        // rate.parcel.dimension.length
        // rate.parcel.dimension.width
        // rate.parcel.dimension.height
        // rate.parcel.dimension.irregularParcelGirth
        
        // rate.rates[0].carrier
        // rate.rates[0].parcelType
        // rate.rates[0].inductionPostalCode
        console.log(rate);
        $scope.getRateReq_final= rate;
        $scope.getShipingRate(rate);

      },function(response){
        console.log('ERROR !!!');
          console.log(response);
      });

     
 };

 


  $scope.getShipingRate = function(req){

    //$scope.generateRequest_GetRates();

    // $http.post("/get-rates").success(function(data){
    console.log('Inside getShipingRate - req --------');
    console.log(req);
    console.log($scope.getRateReq_final);

    $http({
      method: 'POST',
      url: '/get-rates',
      data: req,
      headers: {'Content-Type': 'application/json'}
    }).then(function(data){  
        $scope.loading = false;
        // $scope.customers = data;
        $scope.rate_toSpin=null;

        console.log('Getting Rates');
        console.log(data.data.rates);
        $scope.ratesData= data.data.rates
        // shippingRatesSvc.shippingRat esData=data;
        //console.log(shippingRatesSvc.shippingRatesData);
     // $location.url('/shopify');
    },function(response){
        $scope.seconds = 3;
        $scope.showAlert = true;
        console.log('ERROR !!!');
        console.log(response);
    });
  };

  $scope.printPDFLabel = function(data){
    // $http.post("/print-pdf-label").success(function(pdfdata){
    $scope.ship_toSpin=true;

    $http({
      method: 'POST',
      url: '/print-pdf-label',
      data: data,
      headers: {'Content-Type': 'application/json'}
    }).then(function(data){  
        $scope.loading = false;
        // $scope.customers = data;
        console.log('print-pdf-label');
        console.log('Shipment ID - ');
        console.log(data.data.rates);
        
        $scope.showShipmentDetails = true;

        $scope.ship_toSpin=null;
        
         
        $scope.orderStatus = 'Processed';
        $scope.shipmentId = data.data.shipmentId;
        $scope.parcelTrackingNumber = data.data.parcelTrackingNumber;
        $scope.pdfURL = $sce.trustAsResourceUrl(data.documents[0].contents);
        // $scope.pdfURL = data.data.parcelTrackingNumber;
        
    },function(response){
        $scope.seconds = 3;
        $scope.showAlert = true;
      console.log('ERROR !!!');
      console.log(response);
    });
  };

 


}]);
