'use strict';

angular.module('myApp.shopify-redirect', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/shopify-redirect', {
    templateUrl: 'shopify-redirect/shopify-redirect.html',
    controller: 'shopify-redirectCtrl'
  });
}])

// .controller('shopify-redirectCtrl', ['$scope','$state','$http','$location','$interval',
// function($scope,$state,$http,$location,$interval) {
.factory('shopifySvc', [  
      function(){
        return {
          shopifyData : null
        }
      }

  ])

.controller('shopify-redirectCtrl', ['$scope','$http','$location','shopifySvc',
function($scope,$http,$location,shopifySvc) {
  
  var shop = $location.search().shop;
  var code = $location.search().code;
  $scope.seconds = 3;
  $scope.loading = true;


  // $http.get("/populateOrders?code=" + code + "&shop=" + shop).success(function(data){

  $http({
    method: 'GET',
    url: "/populateOrders?code=" + code + "&shop=" + shop
  }).then(function(data){  
      $scope.loading = false;
      // $scope.customers = data;
      console.log('Getting Orders');
      console.log(data);
      shopifySvc.shopifyData=data.data;
      $location.url('/shopify');
  }, function errorCallback(response) {
    $scope.seconds = 3;
    $scope.showAlert = true;
    console.log('ERROR !!!');
    console.log(response);
  });
  
}]);