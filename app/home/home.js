'use strict';

angular.module('myApp.home', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/home', {
    templateUrl: 'home/home.html',
    controller: 'HomeCtrl'
  });
}])
.controller('shopifyModalCtrl', ['$scope','$window','$uibModalInstance',
function($scope,$window,$uibModalInstance) {
  var baseAddr = $window.location.origin;  
  $scope.toSpin = null;
  $scope.submitShopifyStoreAddress = function(){
    // $window.location.href = 'https://'+$scope.shopifyStoreName+'.myshopify.com/admin/oauth/access_token?email=skjfnjskdf';
    $scope.toSpin = true;
      $window.location.href = 'https://'+$scope.shopifyStoreName+'.myshopify.com/admin/oauth/authorize?client_id=79e0216bb962b309bedc9f0273c384f3&scope=read_orders,write_orders,read_customers&redirect_uri=' + baseAddr + '/shopify-redirect';
   }

   $scope.close = function(){
    $uibModalInstance.dismiss();
   }
}])
.controller('HomeCtrl', ['$scope','$window','$http','$uibModal',
function($scope,$window,$http,$uibModal) {

  var baseAddr = $window.location.origin;   
    $scope.submitShopifyStoreAddress = function(){
    // $window.location.href = 'https://'+$scope.shopifyStoreName+'.myshopify.com/admin/oauth/access_token?email=skjfnjskdf';
      $window.location.href = 'https://'+$scope.shopifyStoreName+'.myshopify.com/admin/oauth/authorize?client_id=79e0216bb962b309bedc9f0273c384f3&scope=read_orders,write_orders,read_customers&redirect_uri=' + baseAddr + '/shopify-redirect';
   }

   var mainAccessToken = '';  
  $scope.openShopify = function(){
    var url = '/home/shopify-modal.html';

    var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: url,
        controller: 'shopifyModalCtrl',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        backdrop: false,
        keyboard:false,
        size: 'lg'
    });


  }
  $scope.getOrderData = function(){
      var addr = 'https://' + $scope.shopifyStoreName + '.myshopify.com/admin/orders.json';
      console.log('URL - '+addr);
      $http({
        url: addr,
        method: 'GET', //Specify the method
        headers: { //We can define headers too
            'Accept': 'application/json',
            'X-Shopify-Access-Token': mainAccessToken
          }
      });

    };




}]);