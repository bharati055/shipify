'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'ngResource',
  'myApp.view1',
  'myApp.home',
  'myApp.shopify',
  'myApp.version',
  'myApp.shopify-redirect'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  // $locationProvider.hashPrefix('!');
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });
  
  $routeProvider.otherwise({redirectTo: '/view1'});

    
}]);

