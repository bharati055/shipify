'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', ['$window',function($window) {
  $scope.gotoHome = function(){
    // $window.location.href = 'https://'+$scope.shopifyStoreName+'.myshopify.com/admin/oauth/access_token?email=skjfnjskdf';
      $window.location.href = '/home';
   }
}]);