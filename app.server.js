var port = process.env.PORT || 3000,
http = require('http'),
bodyParser = require('body-parser'),
express = require('express'),
path = require('path'),
request = require('request');
const uuidv1 = require('uuid/v1');
var app = express();
var router = express.Router();

app.use(express.static('app'));
app.use('/',express.static('app'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use('/node_modules', express.static('node_modules'));

app.use(function(req, res, next) {
res.header("Access-Control-Allow-Origin", "*");
res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
next();
});
app.get('/', function (req, res) {
    console.log('In Root Get Call.');
    res.send('Welcome to Neptune!');
});


var mainAccessToken = null;
var mainShopAddr = null;

app.get('/populateOrders', function (req, res) {
  
          var url = req.url;
          var codeValue = req.query.code;
          var shopAddr = req.query.shop;
          mainShopAddr = shopAddr;
          console.log('Auth code - ' + codeValue + ' and shop name is - ' + shopAddr);
        //   var custDone = false;
        //   var ordersDone = false;
          request.post(
              'https://' + shopAddr + '/admin/oauth/access_token',
              {
                  json:
                  {
                      "client_id": "79e0216bb962b309bedc9f0273c384f3",
                      "client_secret": "eaa21a1fb2aba349f0a5cc111f819fe6",
                      "code": codeValue
                  }
              },
              function (error, response, body) {
                  if (!error && response.statusCode == 200) {
                      var access_token = response.body.access_token;
                      mainAccessToken = access_token;
                      console.log('Get Token Success - Going to Callback function. TOken - '+mainAccessToken);
                      //console.log("Access token generated is  " + access_token);
                      getCustomers(mainAccessToken, shopAddr, res, getAllOrders); // figure out where to call  
  
                  }
              }
          );
  
      });
  
     
      var getCustomers = function (accessToken, shopAddr, res, callback) {
          console.log('Inside get customers - access token - ' + accessToken + '| shop address - ' + shopAddr);
          var jobId = 'Shopify-' + shopAddr.split(".")[0] + new Date().getTime(); 
          request({
              url: 'https://' + shopAddr + '/admin/customers.json',
              method: 'GET', //Specify the method
              headers: { //We can define headers too
                  'Accept': 'application/json',
                  'X-Shopify-Access-Token': accessToken
              }
          }, function (error, response, body) {
              console.log('inside callback of get customers function ');
              if (error) {
                  console.log(error);
              } else {
                 
                      callback(res,accessToken,shopAddr);
               
              }
          });
      };

      var getAllOrders = function (res,mainAccessToken,mainShopAddr) {
        request({
            url: 'https://' + mainShopAddr + '/admin/orders.json',
            method: 'GET', //Specify the method
            headers: { //We can define headers too
                'Accept': 'application/json',
                'X-Shopify-Access-Token': mainAccessToken
            }
        }, function (error, response, body) {
            console.log('inside callback of get getAllOrders function ');
            if (error) {
                console.log('ERROR - '+error);
                res.sendStatus(500);
            } else {
               res.send(body);
               // console.log(body);

            }
        });
    }




app.post('/get-rates', function(req, res, next) {
    var samleRequest = require('./app/common/getRatesServiceReq.json');

    var getRates = function(token){
        var payload = req.body;
        var payload = samleRequest.body;
        //console.log(payload);
        var options = {
                url : 'https://api-sandbox.pitneybowes.com/shippingservices/v1/rates',
                method:"POST",
                json:true,
                headers :{
                    "Authorization" : "Bearer "+token,
                    "content-type" : "application/json"
                },
                body: payload
        };
        request(options, function (err, httpResponse, body) {
            if (err) {
                console.log(err);
                res.sendStatus(500);
            } else {
                console.log("Inside getRates function - response - ");
                console.log(body);
                console.log(payload);
                res.send(body);

            }
        }); 
    };
    getPBToken(getRates);
});

app.post('/print-pdf-label', function(req, res, next) {
    var samlePrintRequest = require('./app/common/printLabelServiceReq.json');

    var printLabel = function(token){
        // var payload = req.body;
        var payload = samlePrintRequest.body;
        console.log('UUID - '+uuidv1().substring(1, 25));
        var options = {
                url : 'https://api-sandbox.pitneybowes.com/shippingservices/v1/shipments',
                method:"POST",
                json:true,
                headers :{
                    "Authorization" : "Bearer "+token,
                    "content-type" : "application/json",
                   //"X-PB-Shipper-Rate-Plan":"PP_SRP_CBP",
                    "X-Pb-TransactionId":uuidv1().substring(1, 25)
                },
                body: payload
        };
        request(options, function (err, httpResponse, body) {
            if (err) {
                console.log('ERROR!!!!! - '+err);
                res.sendStatus(500);
            } else {
                console.log("print-pdf-label service call was successfull !!!!");
                console.log("Inside print-pdf-label function - response - ");
                console.log(body);
                res.send(body);
            }
        }); 
    };
    getPBToken(printLabel);
});

function getPBToken(callback) {
        var options = {
                url : 'https://api-sandbox.pitneybowes.com/oauth/token',
                method:"POST",
                json:true,
                headers :{
                    "content-type" : "application/x-www-form-urlencoded",
                    "Authorization" : "Basic NWRBZURPdXJnMUE1R2RBRjJlcURyQTFuQXFQMERSWlg6NEc1eGVKR3NTeHQ2Y3dyNw==",
                    "grant_type" : "client_credentials"
                },
                form : {
                    grant_type:'client_credentials',
                }
        };
    request(options, function (err, httpResponse, body) {
        if (err) {
            console.log(err);
        } else {
            console.log('Inside - getPBToken - '+body.access_token)
            callback(body.access_token);
        }
    });    
}



app.post('/get-RatesServiceReq-Json', function(req, res, next) {
    var samleRequest = require('./app/common/getRatesServiceReq.json');
    res.send(samleRequest.body);
});




app.get('*', function (req, res) {
    res.sendfile('./app/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});

app.listen(port, function () {
console.log("Listening on port : " + port);
});