# `Shipify` — AngularJS app

This project is an application.
Shipify, SMBs can gather all their orders stored in Shopify/Magento/BigCommerce platforms and they can print the shipping labels from the app itself . 
All information pertaining to the order like the order details ,dimensions ,weight etc. can be fetched from these e-com platforms without any manual intervention.
A rate comparison view of different delivery partners for the SMB customers adds a feather to the user experience . Single platform to complete the order fulfillment flow.